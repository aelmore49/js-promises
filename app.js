const puzzleEl = document.querySelector('#puzzle')
const guessesEl = document.querySelector('#guesses')
const game1 = new Hangman('Car Parts', 2)

puzzleEl.textContent = game1.puzzle
guessesEl.textContent = game1.statusMessage

window.addEventListener('keypress', (e) => {
    const guess = String.fromCharCode(e.charCode)
    game1.makeGuess(guess)
    puzzleEl.textContent = game1.puzzle
    guessesEl.textContent = game1.statusMessage
});
// BELOW IS CALLBACK APPROACH TO GETTING DATA RETURNED  
// BACK FROM A HTTP REQUEST.
getPuzzle((error, puzzle) => {
    if (error) {
        console.log(`Error:${error}`)
    } else {
        console.log(puzzle)
    }

});
getCountry('US', (error, country) => {
    if (error) {
        console.log(`Error:${error}`)
    } else {
        console.log(country.name)
    }

});

// BELOW IS PROMISE APPROACH TO GETTING DATA RETURNED BACK
// FROM A HTTP REQUEST
const myPuzzlePromise = getPuzzlePromise(3);
myPuzzlePromise.then((data) => {
    console.log('This is puzzle data returned back by promise:', data)
}, (error) => {
    console.log('This is the error from the puzzle promise:', error)
})

const myCountryPromise = getCountryPromise('US');
myCountryPromise.then((data) => {
    console.log(`This is the country data returned back from promise: ${data.name}`)
}, (error) => {
    console.log('This is the error from the country promise:', error)
})