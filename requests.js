// SETTING UP FUNCTIONS TO MAKE HTTP REQUEST
// AND PASSING CALLBACK FUNCTIONS TO THEM TO RETURN DATA.
const getPuzzle = (callback) => {
    const request = new XMLHttpRequest();
    request.addEventListener('readystatechange', (e) => {
        if (e.target.readyState === 4 && e.target.status === 200) {
            const data = JSON.parse(e.target.responseText);
            // IF SUCCESSFUL, RETURN FUNCTION WITH 2ND ARGUMENT. 
            callback(undefined, data.puzzle)
        } else if (e.target.readyState === 4) {
            // IF UNSUCCESSFUL, RETURN FUNCTION WITH 1ST ARGUMENT. 
            callback('An error has taken place', undefined)
        }
    });
    request.open('GET', 'http://puzzle.mead.io/puzzle?wordCount=3');
    request.send();

}

const getCountry = (countryCode, callback) => {
    const countryRequest = new XMLHttpRequest();
    countryRequest.addEventListener('readystatechange', (e) => {
        if (e.target.readyState === 4 && e.target.status === 200) {
            const countryData = JSON.parse(e.target.responseText);
            const country = countryData.find((country) => country.alpha2Code === countryCode)
            callback(undefined, country)
        } else if (e.target.readyState === 4) {
            callback('An error has happened!');
        }
    });
    countryRequest.open('GET', 'http://restcountries.eu/rest/v2/all');
    countryRequest.send();
}

// SETTING UP FUNCTIONS TO MAKE HTTP REQUEST
// AND USING PROMISES TO RETURN DATA.
const getPuzzlePromise = (wordCount) => new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.addEventListener('readystatechange', (e) => {
        if (e.target.readyState === 4 && e.target.status === 200) {
            const data = JSON.parse(e.target.responseText);
            // IF SUCCESSFUL, RETURN RESOLVE. 
            resolve(data.puzzle)
        } else if (e.target.readyState === 4) {
            // IF UNSUCCESSFUL, RETURN REJECT. 
            reject('An error has taken place and this is the promise reject.')
        }
    });
    request.open('GET', `http://puzzle.mead.io/puzzle?wordCount=${wordCount}`);
    request.send();
});

const getCountryPromise = (countryCode) => new Promise((resolve, reject) => {
    const countryRequest = new XMLHttpRequest();
    countryRequest.addEventListener('readystatechange', (e) => {
        if (e.target.readyState === 4 && e.target.status === 200) {
            const countryData = JSON.parse(e.target.responseText);
            const country = countryData.find((country) => country.alpha2Code === countryCode)
            resolve(country)
        } else if (e.target.readyState === 4) {
            reject('An error has happened!');
        }
    });
    countryRequest.open('GET', 'http://restcountries.eu/rest/v2/all');
    countryRequest.send();
})